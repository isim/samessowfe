package encentuate.bridges.wfe;

import java.io.ByteArrayInputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Element;

import com.ibm.itim.common.AttributeChangeOperation;
import com.ibm.itim.common.AttributeChanges;
import com.ibm.itim.common.AttributeValue;
import com.ibm.itim.common.AttributeValues;
import com.ibm.itim.dataservices.model.DirectoryObject;
import com.ibm.itim.dataservices.model.DirectoryObjectEntity;
import com.ibm.itim.dataservices.model.DirectoryObjectSearch;
import com.ibm.itim.dataservices.model.DistinguishedName;
import com.ibm.itim.dataservices.model.ModelCommunicationException;
import com.ibm.itim.dataservices.model.ObjectNotFoundException;
import com.ibm.itim.dataservices.model.domain.Account;
import com.ibm.itim.dataservices.model.domain.Service;
import com.ibm.itim.dataservices.model.domain.ServiceEntity;
import com.ibm.itim.dataservices.model.domain.ServiceSearch;
import com.ibm.itim.logging.JLogUtil;
import com.ibm.itim.remoteservices.provider.RequestStatus;
import com.ibm.itim.remoteservices.provider.ServiceProviderInfoOperation;
import com.ibm.itim.remoteservices.provider.ServiceProviderInformation;
import com.ibm.itim.remoteservices.provider.itdiprovider.ItdiServiceProvider;
import com.ibm.log.Level;
import com.ibm.log.PDLogger;

import encentuate.util.Base64;

public class Task {
	private static int reqID = 1000;
	private static final PDLogger traceLogger = JLogUtil.getTraceLogger(ItdiServiceProvider.class);
	private String ErrorMsg = "";
	static final String CLASS_NAME = "Task";
	private static final String ATT_NAME_ACCCRED = "ertamessocredpwd";
	private static final String ATT_NAME_ACCCREDOPER = "ertamessocredoper";
	private static final String SERVICE_PROFILE_NAME = "tamessoprofile";
	private static final String SERVICE_ID_MAPPING = "erservicessomapping";
	private static final String MSG_MAPPING = "ISAM ESSO Authentication Service ID: ";
	private static final String MSG_SERVICE_PROFILE_NAME = "SERVICE_PROFILE_NAME: ";
	private static final String MSG_TENANT_DN = "tenantDN: ";
	private static final String MSG_ENTITY_DN = "entityDN: ";
	private static final String MSG_WF_ID = "WorkFlow RequestID=";
	private static final String MSG_OPERATION = ", operation=";
	private static final String MSG_CRED = ", cred:";
	private static final String MSG_WF_FOR = "Workflow request for ";
	private static final String MSG_RETURN_SUCCESS = " returned: Success";
	private static final String ERR_SERVICE_ID = " Authentication Service ID is not defined ";
	private static final String ERR_RETURN_FAILED = " returned: Failed, ";
	private static final String ERR_SERVICE_NOT_FOUND = "Service is not found for account: ";
	private static final String ERR_SP_INSTANCE_FAILED = "Failed to get instance of Service Provider";
	private static final String ERR_FAILED_TO_FIND_ID_FOR_SERVICE = "Failed to find ISAM E-SSO Authenication Service ID for the ITIM Service Name";
	private static final String ERR_FAILED_TO_FIND_TIM_SERVICE_NAME_FOR_ACC = "Failed to find TIM Serice Name for account";
	private static final String ERR_ATTR_NOT_FOUND = "Unable to find attribute name";
	private static final String ERR_ATTR_EMPTY = "Empty or null attribute name";
	private static final String ERR_CANNOT_PARSE_SSOMAPPING = "Cannot parse ssomapping attribute";

	public boolean taskPwd(Account imsAccount, Account account, String accountPassword, String operation) {
		traceLogger.text(Level.DEBUG_MAX, "Task", "taskPwd", "START ");
		DistinguishedName essoAccountDn = imsAccount.getServiceDN();
		if (essoAccountDn != null) {
			ItdiServiceProvider serviceProvider = this.getServiceProvider(imsAccount);
			if (serviceProvider == null) {
				this.ErrorMsg = this.ErrorMsg + "Failed to get instance of Service Provider";
				traceLogger.text(Level.DEBUG_MIN, "Task", "taskPwd", "Failed to get instance of Service Provider");
				return false;
			} else {
				AttributeChanges var8 = new AttributeChanges();
				AttributeChangeOperation var9 = new AttributeChangeOperation();
				var9.setModificationAction(1);
				Vector var10 = new Vector();
				String var11 = this.getServiceID(account);
				String var12 = this.getAccountDataTemplate(account);
				if (!Util.isNullOrEmpty(var11)) {
					String var13 = "";
					String var14 = "";
					if (!Util.isNullOrEmpty(var12)) {
						String var15 = "";
						String var16 = "";
						var15 = this.getSecondKey(account);
						var16 = Base64.encode(this.getSecondSecret(account));
						var13 = Base64.encode(var11) + "|" + Base64.encode(var12) + "|"
								+ Base64.encode(account.getUserId()) + "|" + accountPassword + "|" + Base64.encode(var15) + "|"
								+ var16;
						var14 = var11 + "|" + var12 + "|" + account.getUserId() + "|" + "******" + "|" + var15 + "|"
								+ "******";
					} else {
						var13 = Base64.encode(var11) + "|" + Base64.encode(account.getUserId()) + "|" + accountPassword;
						var14 = var11 + "|" + account.getUserId() + "|" + "******";
					}

					AttributeValue var19 = new AttributeValue("ertamessocredpwd", var13);
					var10.add(var19);
					AttributeValue var20 = new AttributeValue("ertamessocredoper", operation);
					var10.add(var20);
					var9.setChangeData(var10);
					var8.add(var9);
					String var17 = imsAccount.getDistinguishedName().getAsString();
					traceLogger.text(Level.DEBUG_MAX, "Task", "taskPwd", "entityDN: " + var17 + ", "
							+ "WorkFlow RequestID=" + reqID + ", operation=" + operation + ", cred:" + var14);
					RequestStatus var18 = serviceProvider.modify(var17, var8, "WorkFlow RequestID=" + reqID);
					++reqID;
					if (!var18.failed()) {
						traceLogger.text(Level.DEBUG_MAX, "Task", "taskPwd",
								"Workflow request for " + account.getUserId() + " returned: Success");
						return true;
					} else {
						this.ErrorMsg = this.ErrorMsg + var18.getReasonMessage();
						traceLogger.text(Level.DEBUG_MIN, "Task", "taskPwd",
								"Workflow request for " + account.getUserId() + " returned: Failed, " + this.ErrorMsg);
						return false;
					}
				} else {
					this.ErrorMsg = this.ErrorMsg
							+ "Failed to find ISAM E-SSO Authenication Service ID for the ITIM Service Name";
					traceLogger.text(Level.DEBUG_MIN, "Task", "taskPwd",
							"Failed to find ISAM E-SSO Authenication Service ID for the ITIM Service Name");
					return false;
				}
			}
		} else {
			this.ErrorMsg = this.ErrorMsg + "Failed to find TIM Serice Name for account";
			traceLogger.text(Level.DEBUG_MIN, "Task", "taskPwd", "Failed to find TIM Serice Name for account");
			return false;
		}
	}

	public boolean taskChgPwd(Account imsAccount, Account account, String accountPassword) {
		traceLogger.text(Level.DEBUG_MAX, "Task", "taskChgPwd", "START ");
		return this.taskPwd(imsAccount, account, accountPassword, "modify");
	}

	public boolean taskAddAcc(Account var1, Account var2, String var3) {
		traceLogger.text(Level.DEBUG_MAX, "Task", "taskAddAcc", "START ");
		return this.taskPwd(var1, var2, var3, "add");
	}

	public boolean taskDeleteAcc(Account var1, Account var2) {
		traceLogger.text(Level.DEBUG_MAX, "Task", "taskDeleteAcc", "START ");
		return this.taskPwd(var1, var2, "none", "delete");
	}

	private ItdiServiceProvider getServiceProvider(Account account){
		traceLogger.text(Level.DEBUG_MAX, "Task", "getServiceProvider", "SERVICE_PROFILE_NAME: tamessoprofile");
		Properties props = new Properties();
		String var4 = account.getTenantDN().toString();
		traceLogger.text(Level.DEBUG_MAX, "Task", "getServiceProvider", "tenantDN: " + var4);
		props.setProperty("com.ibm.itim.remoteservices.ResourceProperties.TENANT_DN", var4);
		String serviceProviderFactory = "com.ibm.itim.remoteservices.provider.itdiprovider.ItdiServiceProvider";
		this.setImsServiceProperties(props, account);
		ServiceProviderInformation srvProviderInfo = new ServiceProviderInformation("tamessoprofile", props, 
				(Map<String, ServiceProviderInfoOperation>) null, (HashMap<String, String>)null,
				(Collection<String>) null, (Set<String>) null, serviceProviderFactory);
		return new ItdiServiceProvider(srvProviderInfo);
	}

	public String getServiceID(Account var1) {
		traceLogger.text(Level.DEBUG_MAX, "Task", "getServiceID", "START ");
		String var3 = "";
		Service var4 = this.getServiceForAccount(var1);
		if (var4 == null) {
			traceLogger.text(Level.DEBUG_MIN, "Task", "getServiceID",
					"Service is not found for account: " + var1.getUserId());
			return var3;
		} else {
			String var5 = this.getAuthSvcFromSSOMapping(var4);
			if (var5 != null) {
				return var5;
			} else {
				traceLogger.text(Level.DEBUG_MIN, "Task", "getServiceID", " Authentication Service ID is not defined ");
				return var3;
			}
		}
	}

	public String getAccountDataTemplate(Account var1) {
		traceLogger.text(Level.DEBUG_MAX, "Task", "getAccountDataTemplate", "START ");
		String var3 = "";
		Service var4 = this.getServiceForAccount(var1);
		if (var4 == null) {
			traceLogger.text(Level.DEBUG_MIN, "Task", "getAccountDataTemplate",
					"Service is not found for account: " + var1.getUserId());
			return var3;
		} else {
			String var5 = this.getAccountDataTemplateFromSSOMapping(var4);
			if (var5 != null) {
				return var5;
			} else {
				traceLogger.text(Level.DEBUG_MIN, "Task", "getAccountDataTemplate",
						" Authentication Service ID is not defined ");
				return var3;
			}
		}
	}

	private String getAttributeFromAccountOrService(String var1, Account var2, Service var3) {
		if (!Util.isNullOrEmpty(var1)) {
			if (var1.startsWith("@")) {
				return var1.substring(1);
			} else {
				AttributeValue var5 = var2.getAttribute(var1);
				String var6;
				if (var5 != null) {
					var6 = var5.getValueString();
					return Util.isNullOrEmpty(var6) ? "" : var6;
				} else {
					var5 = var3.getAttribute(var1);
					if (var5 != null) {
						var6 = var5.getValueString();
						return Util.isNullOrEmpty(var6) ? "" : var6;
					} else {
						traceLogger.text(Level.DEBUG_MIN, "Task", "getAttributeFromAccountOrService",
								"Unable to find attribute name: " + var1);
						return "";
					}
				}
			}
		} else {
			traceLogger.text(Level.DEBUG_MIN, "Task", "getAttributeFromAccountOrService",
					"Empty or null attribute name");
			return "";
		}
	}

	public String getSecondKey(Account var1) {
		traceLogger.text(Level.DEBUG_MAX, "Task", "getSecondKey", "START ");
		String var3 = "";
		Service var4 = this.getServiceForAccount(var1);
		if (var4 == null) {
			traceLogger.text(Level.DEBUG_MIN, "Task", "getSecondKey",
					"Service is not found for account: " + var1.getUserId());
			return var3;
		} else {
			String var5 = this.getSecondKeyAttrFromSSOMapping(var4);
			return this.getAttributeFromAccountOrService(var5, var1, var4);
		}
	}

	public String getSecondSecret(Account var1) {
		traceLogger.text(Level.DEBUG_MAX, "Task", "getSecondSecret", "START ");
		String var3 = "";
		Service var4 = this.getServiceForAccount(var1);
		if (var4 == null) {
			traceLogger.text(Level.DEBUG_MIN, "Task", "getSecondSecret",
					"Service is not found for account: " + var1.getUserId());
			return var3;
		} else {
			String var5 = this.getSecondSecretAttrFromSSOMapping(var4);
			return this.getAttributeFromAccountOrService(var5, var1, var4);
		}
	}

	private String simpleXMLParser(String var1, String var2) {
		traceLogger.text(Level.DEBUG_MAX, "Task", "simpleXMLParser", "START ");

		try {
			DocumentBuilder var4 = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Element var5 = var4.parse(new ByteArrayInputStream(var1.getBytes("UTF-8"))).getDocumentElement();
			if (var5.hasChildNodes()) {
				traceLogger.text(Level.DEBUG_MIN, "Task", "simpleXMLParser",
						"Cannot parse ssomapping attribute: Node has child elements");
				return null;
			} else if (var5.hasAttribute(var2)) {
				return var5.getAttribute(var2);
			} else {
				traceLogger.text(Level.DEBUG_MIN, "Task", "simpleXMLParser",
						"Cannot parse ssomapping attribute: Attribute name not found");
				return null;
			}
		} catch (Exception var6) {
			traceLogger.text(Level.DEBUG_MIN, "Task", "simpleXMLParser",
					"Cannot parse ssomapping attribute: 4. " + var6.getMessage());
			return null;
		}
	}

	private String getAccountDataTemplateFromSSOMapping(Service var1) {
		AttributeValue var3 = var1.getAttribute("erservicessomapping");
		traceLogger.text(Level.DEBUG_MAX, "Task", "getAccountDataTemplateFromSSOMapping",
				"ISAM ESSO Authentication Service ID: " + (var3 == null ? "null" : var3.toString()));
		if (var3 != null) {
			String var4 = var3.getValueString();
			String var5 = "adt";
			return this.simpleXMLParser(var4, var5);
		} else {
			traceLogger.text(Level.DEBUG_MIN, "Task", "getAccountDataTemplateFromSSOMapping",
					" Authentication Service ID is not defined ");
			return null;
		}
	}

	private String getSecondKeyAttrFromSSOMapping(Service var1) {
		if (var1 == null) {
			traceLogger.text(Level.DEBUG_MIN, "Task", "getSecondKeyAttrFromSSOMapping", "Null Service");
			return null;
		} else {
			AttributeValue var3 = var1.getAttribute("erservicessomapping");
			traceLogger.text(Level.DEBUG_MAX, "Task", "getSecondKeyAttrFromSSOMapping",
					"ISAM ESSO Authentication Service ID: " + (var3 == null ? "null" : var3.toString()));
			if (var3 != null) {
				String var4 = var3.getValueString();
				String var5 = "secondkeyattr";
				return this.simpleXMLParser(var4, var5);
			} else {
				traceLogger.text(Level.DEBUG_MIN, "Task", "getSecondKeyAttrFromSSOMapping",
						" Authentication Service ID is not defined ");
				return null;
			}
		}
	}

	private String getSecondSecretAttrFromSSOMapping(Service var1) {
		if (var1 == null) {
			traceLogger.text(Level.DEBUG_MIN, "Task", "getSecondSecretAttrFromSSOMapping", "Null Service");
			return null;
		} else {
			AttributeValue var3 = var1.getAttribute("erservicessomapping");
			traceLogger.text(Level.DEBUG_MAX, "Task", "getSecondSecretAttrFromSSOMapping",
					"ISAM ESSO Authentication Service ID: " + (var3 == null ? "null" : var3.toString()));
			if (var3 != null) {
				String var4 = var3.getValueString();
				String var5 = "secondsecretattr";
				return this.simpleXMLParser(var4, var5);
			} else {
				traceLogger.text(Level.DEBUG_MIN, "Task", "getSecondSecretAttrFromSSOMapping",
						" Authentication Service ID is not defined ");
				return null;
			}
		}
	}

	private String getAuthSvcFromSSOMapping(Service var1) {
		AttributeValue var3 = var1.getAttribute("erservicessomapping");
		traceLogger.text(Level.DEBUG_MAX, "Task", "getAuthSvcFromSSOMapping",
				"ISAM ESSO Authentication Service ID: " + (var3 == null ? "null" : var3.toString()));
		if (var3 != null) {
			String var4 = var3.getValueString();
			String var5 = "asi";
			String var6 = this.simpleXMLParser(var4, var5);
			return !Util.isNullOrEmpty(var6) ? var6 : var4;
		} else {
			traceLogger.text(Level.DEBUG_MIN, "Task", "getAuthSvcFromSSOMapping",
					" Authentication Service ID is not defined ");
			return null;
		}
	}

	public Service getServiceForAccount(Account account) {
		traceLogger.text(Level.DEBUG_MAX, "Task", "getServiceForAccount", "START ");

		try {
			ServiceEntity serviceEntity = (new ServiceSearch()).lookup(account.getServiceDN());
			return (Service) serviceEntity.getDirectoryObject();
		} catch (ObjectNotFoundException var4) {
			traceLogger.exception(Level.DEBUG_MIN, "Task", "getServiceForAccount", var4);
			return null;
		} catch (ModelCommunicationException var5) {
			traceLogger.exception(Level.DEBUG_MIN, "Task", "getServiceForAccount", var5);
			return null;
		}
	}

	private void setImsServiceProperties(Properties props, Account account) {
		traceLogger.text(Level.DEBUG_MAX, "Task", "setImsServiceProperties", "START ");
		DistinguishedName accountServiceDn = account.getServiceDN();
		traceLogger.text(Level.DEBUG_MAX, "Task", "setImsServiceProperties", accountServiceDn.getAsString());
		if (accountServiceDn != null) {
			DirectoryObjectSearch var5 = new DirectoryObjectSearch();

			try {
				DirectoryObjectEntity var6 = var5.lookup(accountServiceDn);
				DirectoryObject var7 = var6.getDirectoryObject();
				AttributeValues var8 = var7.getAttributes();
				this.attr2prop("ertamessodbname", var8, props);
				this.attr2prop("ertamessodblogin", var8, props);
				this.attr2prop("ertamessoservername", var8, props);
				this.attr2prop("erservicepwd2", var8, props);
				this.attr2prop("erservicepwd1", var8, props);
				this.attr2prop("erparent", var8, props);
				this.attr2prop("ertamessobridgename", var8, props);
				this.attr2prop("erglobalid", var8, props);
				this.attr2prop("eritdiurl", var8, props);
				this.attr2prop("erservicename", var8, props);
				this.attr2prop("com.ibm.itim.remoteservices.ResourceProperties.TENANT_DN", var8, props);
				this.attr2prop("ertamessoalgo", var8, props);
				this.attr2prop("ertamessotrans", var8, props);
				this.attr2prop("ertamessoport", var8, props);
				traceLogger.text(Level.DEBUG_MAX, "Task", "setImsServiceProperties", "Service Properties" + props);
			} catch (ObjectNotFoundException var9) {
				traceLogger.exception(Level.DEBUG_MIN, "Task", "setImsServiceProperties", var9, var9.getMessage());
			} catch (ModelCommunicationException var10) {
				traceLogger.exception(Level.DEBUG_MIN, "Task", "setImsServiceProperties", var10, var10.getMessage());
			} catch (Exception var11) {
				traceLogger.exception(Level.DEBUG_MIN, "Task", "setImsServiceProperties", var11, var11.getMessage());
			}
		}

	}

	private void attr2prop(String var1, AttributeValues var2, Properties var3) {
		AttributeValue var4 = var2.get(var1);
		if (var4 != null) {
			var3.setProperty(var1, var4.getValueString());
		}

	}
}