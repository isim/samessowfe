package encentuate.bridges.wfe;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.ibm.itim.dataservices.model.DistinguishedName;
import com.ibm.itim.dataservices.model.ModelCommunicationException;
import com.ibm.itim.dataservices.model.ModelIntegrityException;
import com.ibm.itim.dataservices.model.domain.Account;
import com.ibm.itim.dataservices.model.domain.Person;
import com.ibm.itim.dataservices.model.domain.PersonEntity;
import com.ibm.itim.dataservices.model.domain.RoleEntity;
import com.ibm.itim.dataservices.model.domain.Service;
import com.ibm.itim.logging.JLogUtil;
import com.ibm.itim.remoteservices.provider.itdiprovider.ItdiServiceProvider;
import com.ibm.itim.util.EncryptionManager;
import com.ibm.itim.workflow.model.ActivityResult;
import com.ibm.log.Level;
import com.ibm.log.PDLogger;

import encentuate.util.Base64;

public class IMSAgent {
	private static final String MSG_PROFILE = ", Profile: ";
	private static final String MSG_OWNER = ", Owner: ";
	private static final String MSG_ACC_ADDED = "Add credential into ISAM E-SSO account's wallet completed successfully";
	private static final String MSG_ACC_CHANGEPWD = "Change credential's password in ISAM E-SSO account's wallet completed successfully";
	private static final String MSG_ACC_DELETE = "Delete credential from ISAM E-SSO account's wallet completed successfully";
	private static final String ERR_ADD_FAILED = " Failed to add credential into ISAMESSO Account's Wallet: ";
	private static final String ERR_CHANGE_FAILED = "Failed to change Password of a credential in ISAMESSO Account's Wallet: ";
	private static final String ERR_DELETE_FAILED = "Failed to delete credential from a ISAMESSO Account's Wallet: ";
	private static final String ERR_IMS_ACC_NOT_FOUND = "Failed to find the corresponding ISAM E-SSO account";
	private static final String ERR_IMS_ID_NOT_FOUND = "ISAM E-SSO user ID is not found";
	private static final String ERR_ACC_OWNER_NOT_FOUND = "Account owner not found";
	private static final String ERR_SERVICE_SR = " Service is not found ";
	private static final String ERR_ROLE_NOT_FOUND = " Role is not found ";
	private static final String ERR_SHARED_ACCOUNT_NOT_FOUND = " Shared account is not found ";
	private Task task = new Task();
	private Util util = new Util();
	private static final PDLogger traceLogger = JLogUtil.getTraceLogger(ItdiServiceProvider.class);
	private String ErrorMsg = "";
	static final String CLASS_NAME = "IMSAgent";

	public ActivityResult encAddAccount(Person person, Service service, Account account) {
		traceLogger.text(Level.DEBUG_MAX, "IMSAgent", "encAddAccount", "");
		String var5 = "";
		String var6 = this.getBase64Password(account.getPassword());
		if (!this.util.isImsAccount(account.getProfileName())) {
			try {
				Account var7 = this.util.getImsAccount(service, person.getDistinguishedName());
				if (var7 == null) {
					return new ActivityResult("SF",
							"Failed to find the corresponding ISAM E-SSO account" + this.ErrorMsg, (List) null);
				}

				var5 = var7.getUserId();
				if (var5 == null) {
					return new ActivityResult("SF", "ISAM E-SSO user ID is not found" + this.ErrorMsg, (List) null);
				}

				if (!this.task.taskAddAcc(var7, account, var6)) {
					return new ActivityResult("SF",
							" Failed to add credential into ISAMESSO Account's Wallet: " + this.ErrorMsg, (List) null);
				}
			} catch (Exception var8) {
				traceLogger.exception(Level.DEBUG_MIN, "IMSAgent", "encAddAccount", var8);
				return new ActivityResult("SF", var8.getClass().getName() + ": " + var8.getMessage(), (List) null);
			}
		}

		return new ActivityResult(2, "SS", "Add credential into ISAM E-SSO account's wallet completed successfully",
				(List) null);
	}

	public ActivityResult encChangePassword(Account account) {
		traceLogger.text(Level.DEBUG_MAX, "IMSAgent", "encChangePasswordWithOwner", "");
		ActivityResult activityResult = null;
		traceLogger.text(Level.DEBUG_MAX, "IMSAgent", "encChangePasswordWithOwner",
				", Profile: " + account.getProfileName() + ", Owner: " + account.getOwnerDN());
		if (account.getOwnerDN() != null) {
			activityResult = this.encChangePasswordWithOwner(account, account.getOwnerDN());
		} else {
			activityResult = new ActivityResult("SF", "Account owner not found" + this.ErrorMsg, (List) null);
		}

		return activityResult;
	}

	public ActivityResult encChangePasswordWithOwner(Account account, DistinguishedName personDn) {
		traceLogger.text(Level.DEBUG_MAX, "IMSAgent", "encChangePasswordWithOwner", "");
		String imsEruid = "";
		String accountPassword = this.getBase64Password(account.getPassword());
		if (!this.util.isImsAccount(account.getProfileName())) {
			Service service = this.task.getServiceForAccount(account);
			if (service == null) {
				return new ActivityResult("SF", " Service is not found " + this.ErrorMsg, (List) null);
			}

			Account imsAccount = this.util.getImsAccount(service, personDn);
			if (imsAccount == null) {
				return new ActivityResult("SF", "Failed to find the corresponding ISAM E-SSO account" + this.ErrorMsg,
						(List) null);
			}

			imsEruid = imsAccount.getUserId();
			if (imsEruid == null) {
				return new ActivityResult("SF", "ISAM E-SSO user ID is not found" + this.ErrorMsg, (List) null);
			}

			if (!this.task.taskChgPwd(imsAccount, account, accountPassword)) {
				return new ActivityResult("SF",
						"Failed to change Password of a credential in ISAMESSO Account's Wallet: " + this.ErrorMsg,
						(List) null);
			}
		}

		return new ActivityResult(2, "SS",
				"Change credential's password in ISAM E-SSO account's wallet completed successfully", (List) null);
	}

	public ActivityResult encDeleteAccountWithOwner(Account var1, DistinguishedName var2) {
		traceLogger.text(Level.DEBUG_MAX, "IMSAgent", "encDeleteAccountWithOwner", "");
		ActivityResult var4 = null;
		String var5 = "";
		if (!this.util.isImsAccount(var1.getProfileName())) {
			Service var6 = this.task.getServiceForAccount(var1);
			if (var6 == null) {
				return new ActivityResult("SF", " Service is not found " + this.ErrorMsg, (List) null);
			}

			traceLogger.text(Level.DEBUG_MAX, "IMSAgent", "encDeleteAccountWithOwner", ", Owner: " + var2);
			Account var7 = this.util.getImsAccount(var6, var2);
			if (var7 != null) {
				var5 = var7.getUserId();
				if (var5 != null) {
					if (!this.task.taskDeleteAcc(var7, var1)) {
						var4 = new ActivityResult("SF",
								"Failed to delete credential from a ISAMESSO Account's Wallet: " + this.ErrorMsg,
								(List) null);
					} else {
						var4 = new ActivityResult(2, "SS",
								"Delete credential from ISAM E-SSO account's wallet completed successfully",
								(List) null);
					}
				} else {
					var4 = new ActivityResult("SF", "ISAM E-SSO user ID is not found" + this.ErrorMsg, (List) null);
				}
			} else {
				var4 = new ActivityResult("SF", "Failed to find the corresponding ISAM E-SSO account" + this.ErrorMsg,
						(List) null);
			}
		} else {
			var4 = new ActivityResult(2, "SS",
					"Delete credential from ISAM E-SSO account's wallet completed successfully", (List) null);
		}

		return var4;
	}

	public ActivityResult encDeleteAccount(Account var1) {
		traceLogger.text(Level.DEBUG_MAX, "IMSAgent", "encDeleteAccount", "");
		ActivityResult var3 = null;
		traceLogger.text(Level.DEBUG_MAX, "IMSAgent", "encDeleteAccount",
				", Profile: " + var1.getProfileName() + ", Owner: " + var1.getOwnerDN());
		if (var1.getOwnerDN() != null) {
			var3 = this.encDeleteAccountWithOwner(var1, var1.getOwnerDN());
		} else {
			var3 = new ActivityResult("SF", "Account owner not found" + this.ErrorMsg, (List) null);
		}

		return var3;
	}

	public ActivityResult encChangeSharedAccountPassword(Account var1) {
		traceLogger.text(Level.DEBUG_MAX, "IMSAgent", "encChangeSharedAccountPassword", "");
		String var3 = "";
		String var4 = this.getBase64Password(var1.getPassword());
		if (!this.isSharedAccountForIMS(var1)) {
			traceLogger.text(Level.DEBUG_MIN, "IMSAgent", "encChangeSharedAccountPassword",
					" Shared account is not found ");
			return this.encChangePassword(var1);
		} else {
			RoleEntity var5 = this.util.getRoleforSharedAccount(var1);
			if (var5 == null) {
				traceLogger.text(Level.DEBUG_MIN, "IMSAgent", "encChangeSharedAccountPassword", " Role is not found ");
				return new ActivityResult("SF", " Role is not found " + this.ErrorMsg, (List) null);
			} else {
				Collection var6 = this.getSharedRoleMembers(var5);
				Iterator var7 = var6.iterator();

				while (var7.hasNext()) {
					PersonEntity var8 = (PersonEntity) var7.next();
					Collection var9 = this.util.findImsAccounts(var8.getDistinguishedName());
					Iterator var10 = var9.iterator();

					while (var10.hasNext()) {
						Account var11 = (Account) var10.next();
						if (var11 != null) {
							var3 = var11.getUserId();
							if (var3 == null) {
								traceLogger.text(Level.DEBUG_MIN, "IMSAgent", "encChangeSharedAccountPassword",
										"Failed to find the corresponding ISAM E-SSO account");
							} else {
								traceLogger.text(Level.DEBUG_MAX, "IMSAgent", "encChangeSharedAccountPassword",
										" TAMESSO account=" + var11.getName() + "|shareAccount=" + var1.getUserId());
								if (!this.task.taskChgPwd(var11, var1, var4)) {
									return new ActivityResult("SF",
											"Failed to change Password of a credential in ISAMESSO Account's Wallet: "
													+ this.ErrorMsg,
											(List) null);
								}
							}
						}
					}
				}

				return new ActivityResult(2, "SS",
						"Change credential's password in ISAM E-SSO account's wallet completed successfully",
						(List) null);
			}
		}
	}

	private String getBase64Password(byte[] var1) {
		String var2 = new String(EncryptionManager.getInstance().decrypt(var1));
		return Base64.encode(var2);
	}

	private boolean isSharedAccountForIMS(Account var1) {
		traceLogger.text(Level.DEBUG_MAX, "IMSAgent", "isSharedAccountForIMS", "");
		Service var3 = this.task.getServiceForAccount(var1);
		if (var3 != null && !this.util.isImsService(var3.getProfileName())) {
			Service var4 = this.util.findTAMESSOPrerequisite(var3);
			if (var4 != null && this.util.isSharedAccount(var1, var4)) {
				return true;
			}
		}

		return false;
	}

	private Collection getSharedRoleMembers(RoleEntity var1) {
		traceLogger.text(Level.DEBUG_MAX, "IMSAgent", "getSharedRoleMembers", "");
		new ArrayList();
		ArrayList var4 = new ArrayList();

		try {
			Collection var3 = var1.getMembers();
			Iterator var5 = var3.iterator();

			while (var5.hasNext()) {
				PersonEntity var6 = (PersonEntity) var5.next();
				var4.add(var6);
			}
		} catch (ModelCommunicationException var7) {
			traceLogger.exception(Level.DEBUG_MIN, "IMSAgent", "getSharedRoleMembers", var7);
		} catch (ModelIntegrityException var8) {
			traceLogger.exception(Level.DEBUG_MIN, "IMSAgent", "getSharedRoleMembers", var8);
		}

		return var4;
	}
}