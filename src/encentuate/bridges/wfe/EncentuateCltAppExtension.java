package encentuate.bridges.wfe;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import com.ibm.itim.common.AttributeChangeOperation;
import com.ibm.itim.common.AttributeValue;
import com.ibm.itim.dataservices.model.DistinguishedName;
import com.ibm.itim.dataservices.model.ModelCommunicationException;
import com.ibm.itim.dataservices.model.ObjectNotFoundException;
import com.ibm.itim.dataservices.model.domain.Account;
import com.ibm.itim.dataservices.model.domain.AccountEntity;
import com.ibm.itim.dataservices.model.domain.AccountSearch;
import com.ibm.itim.dataservices.model.domain.Person;
import com.ibm.itim.dataservices.model.domain.PersonEntity;
import com.ibm.itim.dataservices.model.domain.PersonSearch;
import com.ibm.itim.dataservices.model.domain.Role;
import com.ibm.itim.dataservices.model.domain.Service;
import com.ibm.itim.logging.JLogUtil;
import com.ibm.itim.remoteservices.provider.itdiprovider.ItdiServiceProvider;
import com.ibm.itim.workflow.application.WorkflowApplication;
import com.ibm.itim.workflow.application.WorkflowExecutionContext;
import com.ibm.itim.workflow.model.ActivityResult;
import com.ibm.log.Level;
import com.ibm.log.PDLogger;

public class EncentuateCltAppExtension implements WorkflowApplication {
	public static final String EXT_NAME = "IBM Security Access Manager E-SSO Workflow Extension version ";
	public static final String VERSION = "6.0.3.18";
	private static final String ROLE_TYPE = "erroles";
	private static final String ROLE_NAME = "roleName=";
	private static final String SHARED_ROLE = "sharedRole=";
	private static final String ROLE_OWNER = "Role Owner=";
	private static final String SEND_EMAIL = "sendEmail=";
	private static final String RECIPIENT = "Recipient=";
	private static final String MSG_ACC_ADDED = "Add credential into account's wallet completed successfully";
	private static final String MSG_ACC_CHANGEPWD = "Change credential's password in ISAM E-SSO account's wallet completed successfully";
	private static final String MSG_ACC_DELETE = "Delete credential from ISAM E-SSO account's wallet completed successfully";
	private static final String MSG_ONLY_ONE_ACC = "Only one ISAM E-SSO account per service is allowed.";
	private static final String MSG_TAMESSO_ACC_FOUND = "Found existing ISAMESSO Account : ";
	private static final String MSG_SHARED_ACCOUNT_REMOVED = " Shared account is removed from ISAM ESSO account's wallet";
	private static final String MSG_SHARED_ROLE_FOUND = "A role is found for the shared account : ";
	private static final String MSG_ROLE_OWNER = "User is owner of the role for shared account ";
	private static final String MSG_NO_PREREQ_FOUND = "No ISAMESSO prerequisite service found";
	private static final String ERR_SHARED_ROLE_NOT_FOUND = "No role found for the shared account";
	private static final String ERR_REMOVE_SHARED_ACCOUNT = "Failed to remove shared account from ISAM ESSO account's wallet";
	private static final String ERR_ACTUAL_ROLE_NOT_FOUND = "Role is not found";
	private static final String ERR_ROLE_OWNER_NOT_FOUND = "Role doesn't have a owner";
	private static final String ERR_TAMESSO_ACC_NOT_FOUND = "ISAM ESSO account is not found";
	private static final String ERR_OWNERSHIPTYPE_WRONG = "Account Ownership Type must be Individual for access in IMS wallet";
	private static final String MSG_ACC_NOT_MANAGED_BY_IMS = "Account has Ownership Type that is not managed by ISAM ESSO Service";
	private static final String MSG_ACC_IMS_MANAGED = "Account Owernership Type is managed by ISAM ESSO Service";
	private static final String MSG_ACC_SERVICE_NOT_INTEGRATED_WITH_IMS = "ISAM ESSO service prerequisite not configured";
	private Task task;
	private Util util;
	private IMSAgent imsAgent;
	private static final PDLogger traceLogger = JLogUtil.getTraceLogger(ItdiServiceProvider.class);
	static final String CLASS_NAME = "EncentuateCltAppExtension";
	protected WorkflowExecutionContext wfCtx;

	public EncentuateCltAppExtension() {
		this.task = new Task();
		this.util = new Util();
		this.imsAgent = new IMSAgent();
	}

	public EncentuateCltAppExtension(WorkflowExecutionContext var1) {
		this.setContext(var1);
		this.task = new Task();
		this.util = new Util();
		this.imsAgent = new IMSAgent();
	}

	public void setContext(WorkflowExecutionContext var1) {
		this.wfCtx = var1;
	}

	public ActivityResult isAccountIMSManaged(Account account) {
		Service services = this.task.getServiceForAccount(account);
		Service isamService = this.util.findTAMESSOPrerequisite(services);
		this.LogDebug(Level.DEBUG_MIN, "EncentuateCltAppExtension", "isAccountIMSManaged",
				"Service for account: " + services.getName());

		if ("Individual".equalsIgnoreCase(account.getOwnershipType())) {
			this.LogDebug(Level.DEBUG_MAX, "EncentuateCltAppExtension", "isAccountIMSManaged",
					"Account Owernership Type is managed by ISAM ESSO Service");
			return new ActivityResult("SS", "Account is managed by IMS", (List) null);
		}

		if (isamService != null) {
			this.LogDebug(Level.DEBUG_MIN, "EncentuateCltAppExtension", "isAccountIMSManaged",
					"Service for account: " + services.getName() + " has essoPrereq service: " + isamService.getName());
			Iterator var5 = (new Vector()).iterator();

			try {
				Collection var6 = isamService.getAttribute("ertamessoownership").getValues();
				var5 = var6.iterator();
			} catch (Exception var7) {
				this.LogDebug(Level.DEBUG_MAX, "EncentuateCltAppExtension", "isAccountIMSManaged", var7.getMessage());
			}

			String var8;
			do {
				if (!var5.hasNext()) {
					this.LogDebug(Level.DEBUG_MAX, "EncentuateCltAppExtension", "isAccountIMSManaged",
							"Account has Ownership Type that is not managed by ISAM ESSO Service");
					return new ActivityResult("SK",
							"Account has Ownership Type that is not managed by ISAM ESSO Service", (List) null);
				}

				var8 = (String) var5.next();
			} while (!var8.equalsIgnoreCase(account.getOwnershipType()));

			this.LogDebug(Level.DEBUG_MAX, "EncentuateCltAppExtension", "isAccountIMSManaged",
					"Account Owernership Type is managed by ISAM ESSO Service");
			return new ActivityResult("SS", "Account Owernership Type is managed by ISAM ESSO Service", (List) null);

		} else {
			this.LogDebug(Level.DEBUG_MAX, "EncentuateCltAppExtension", "isAccountIMSManaged",
					MSG_ACC_SERVICE_NOT_INTEGRATED_WITH_IMS);
			return new ActivityResult("SK", MSG_ACC_SERVICE_NOT_INTEGRATED_WITH_IMS, (List) null);
		}
	}

	public ActivityResult createITIMAccountWithTAMESSO(Person var1, Service var2, Account var3) {
		traceLogger.text(Level.DEBUG_MIN, "EncentuateCltAppExtension", "createITIMAccountWithTAMESSO",
				"IBM Security Access Manager E-SSO Workflow Extension version 6.0.3.18");
		return this.createAccountWithTAMESSO(var1, var2, var3);
	}

	private void LogDebug(Level var1, String var2, String var3, String var4) {
		traceLogger.text(var1, var2, var3, var4);
	}

	public ActivityResult createAdoptedAccountWithTAMESSO(Person var1, Account var2) {
		traceLogger.text(Level.DEBUG_MIN, "EncentuateCltAppExtension", "createAdoptedAccountWithTAMESSO",
				"IBM Security Access Manager E-SSO Workflow Extension version 6.0.3.18");

		try {
			Service var4 = this.task.getServiceForAccount(var2);
			if (this.hasImsAccountPerService(var1, var4)) {
				this.LogDebug(Level.DEBUG_MIN, "EncentuateCltAppExtension", "createAdoptedAccountWithTAMESSO",
						"Failed hasImsAccountPerService test.");
				return new ActivityResult("SF", "Only one ISAM E-SSO account per service is allowed.", (List) null);
			} else {
				String var5 = "";
				this.LogDebug(Level.DEBUG_MIN, "EncentuateCltAppExtension", "createAdoptedAccountWithTAMESSO",
						"Checking TAMESSO prereq");
				ActivityResult var6 = this.isAccountIMSManaged(var2);
				if (var6.getSummary().equals("SS")) {
					this.LogDebug(Level.DEBUG_MIN, "EncentuateCltAppExtension", "createAdoptedAccountWithTAMESSO",
							"calling encAddAccount");
					ActivityResult var7 = this.imsAgent.encAddAccount(var1, var4, var2);
					if (var7.getSummary().compareToIgnoreCase("SF") == 0) {
						var7.setSummary("SW");
						return var7;
					}

					if (!Util.isNullOrEmpty(var5)) {
						var5 = var5 + " " + "Add credential into account's wallet completed successfully";
					} else {
						var5 = "Add credential into account's wallet completed successfully";
					}
				} else {
					var5 = var6.getDescription();
				}

				return new ActivityResult(2, "SS", var5, (List) null);
			}
		} catch (Exception var8) {
			traceLogger.exception(Level.DEBUG_MIN, "EncentuateCltAppExtension", "createAdoptedAccountWithTAMESSO",
					var8);
			return new ActivityResult("SF", var8.getClass().getName() + ": " + var8.getMessage(), (List) null);
		}
	}

	public ActivityResult accountAdoptionWithTAMESSO(Person var1, Account var2) {
		traceLogger.text(Level.DEBUG_MIN, "EncentuateCltAppExtension", "accountAdoptionWithTAMESSO",
				"IBM Security Access Manager E-SSO Workflow Extension version 6.0.3.18");
		return this.isAccountIMSManaged(var2).getSummary().equals("SS")
				? new ActivityResult(2, "SS", "Skipped", (List) null)
				: this.deleteAdoptedAccountWithTAMESSO(var1, var2);
	}

	public ActivityResult createAccountWithTAMESSO(Person var1, Service var2, Account var3) {
		traceLogger.text(Level.DEBUG_MIN, "EncentuateCltAppExtension", "createAccountWithTAMESSO",
				"IBM Security Access Manager E-SSO Workflow Extension version 6.0.3.18");

		try {
			if (this.hasImsAccountPerService(var1, var2)) {
				this.LogDebug(Level.DEBUG_MIN, "EncentuateCltAppExtension", "createAccountWithTAMESSO",
						"Failed hasImsAccountPerService test.");
				return new ActivityResult("SF", "Only one ISAM E-SSO account per service is allowed.", (List) null);
			} else {
				String var5 = "";
				this.LogDebug(Level.DEBUG_MIN, "EncentuateCltAppExtension", "createAccountWithTAMESSO",
						"Checking TAMESSO prereq");
				ActivityResult var6 = this.isAccountIMSManaged(var3);
				if (var6.getSummary().equals("SS")) {
					this.LogDebug(Level.DEBUG_MIN, "EncentuateCltAppExtension", "createAccountWithTAMESSO",
							"calling encAddAccount");
					ActivityResult var7 = this.imsAgent.encAddAccount(var1, var2, var3);
					if (var7.getSummary().compareToIgnoreCase("SF") == 0) {
						var7.setSummary("SW");
						return var7;
					}

					if (!Util.isNullOrEmpty(var5)) {
						var5 = var5 + " " + "Add credential into account's wallet completed successfully";
					} else {
						var5 = "Add credential into account's wallet completed successfully";
					}
				} else {
					var5 = var6.getDescription();
				}

				return new ActivityResult(2, "SS", var5, (List) null);
			}
		} catch (Exception var8) {
			traceLogger.exception(Level.DEBUG_MIN, "EncentuateCltAppExtension", "createAccountWithTAMESSO", var8);
			return new ActivityResult("SF", var8.getClass().getName() + ": " + var8.getMessage(), (List) null);
		}
	}

	public ActivityResult deleteAdoptedAccountWithTAMESSO(Person var1, Account var2) {
		traceLogger.text(Level.DEBUG_MIN, "EncentuateCltAppExtension", "deleteAdoptedAccountWithTAMESSO",
				"IBM Security Access Manager E-SSO Workflow Extension version 6.0.3.18");

		try {
			String var4 = "";
			if (this.util.hasTAMESSOPrerequisite(var2)) {
				ActivityResult var5 = this.imsAgent.encDeleteAccountWithOwner(var2, var1.getDistinguishedName());
				if (var5.getSummary().compareToIgnoreCase("SF") == 0) {
					var5.setSummary("SW");
					return var5;
				}

				if (var4 != null && !var4.equals("")) {
					var4 = var4 + " " + "Delete credential from ISAM E-SSO account's wallet completed successfully";
				} else {
					var4 = "Delete credential from ISAM E-SSO account's wallet completed successfully";
				}
			} else {
				var4 = MSG_NO_PREREQ_FOUND;
			}

			return new ActivityResult(2, "SS", var4, (List) null);
		} catch (Exception var6) {
			traceLogger.exception(Level.DEBUG_MIN, "EncentuateCltAppExtension", "deleteAdoptedAccountWithTAMESSO",
					var6);
			return new ActivityResult("SF", var6.getClass().getName() + ": " + var6.getMessage(), (List) null);
		}
	}

	public ActivityResult deleteAccountWithTAMESSO(Account account) {
		traceLogger.text(Level.DEBUG_MIN, "EncentuateCltAppExtension", "deleteAccountWithTAMESSO",
				"IBM Security Access Manager E-SSO Workflow Extension version 6.0.3.18");

		try {
			String activityMsg = "";
			if (this.util.hasTAMESSOPrerequisite(account)) {
				ActivityResult var4 = this.imsAgent.encDeleteAccount(account);
				if (var4.getSummary().compareToIgnoreCase("SF") == 0) {
					var4.setSummary("SW");
					return var4;
				}

				if (activityMsg != null && !activityMsg.equals("")) {
					activityMsg = activityMsg + " " + "Delete credential from ISAM E-SSO account's wallet completed successfully";
				} else {
					activityMsg = "Delete credential from ISAM E-SSO account's wallet completed successfully";
				}
			} else {
				activityMsg = MSG_NO_PREREQ_FOUND;
			}

			return new ActivityResult(2, "SS", activityMsg, (List) null);
		} catch (Exception var5) {
			traceLogger.exception(Level.DEBUG_MIN, "EncentuateCltAppExtension", "deleteAccountWithTAMESSO", var5);
			return new ActivityResult("SF", var5.getClass().getName() + ": " + var5.getMessage(), (List) null);
		}
	}

	public ActivityResult changePasswordWithTAMESSO(Account account, String var2) {
		traceLogger.text(Level.DEBUG_MIN, "EncentuateCltAppExtension", "changePasswordWithTAMESSO",
				"IBM Security Access Manager E-SSO Workflow Extension version 6.0.3.18");

		try {
			String var4 = "";
			ActivityResult var5 = this.isAccountIMSManaged(account);
			if (var5.getSummary().equals("SS")) {
				ActivityResult var6 = this.imsAgent.encChangePassword(account);
				if (var6.getSummary().compareToIgnoreCase("SF") == 0) {
					var6.setSummary("SW");
					return var6;
				}

				if (!Util.isNullOrEmpty(var4)) {
					var4 = var4 + " "
							+ "Change credential's password in ISAM E-SSO account's wallet completed successfully";
				} else {
					var4 = "Change credential's password in ISAM E-SSO account's wallet completed successfully";
				}
			} else {
				var4 = var5.getDescription();
			}

			return new ActivityResult(2, "SS", var4, (List) null);
		} catch (Exception e) {
			traceLogger.exception(Level.DEBUG_MIN, "EncentuateCltAppExtension", "changePasswordWithTAMESSO", e);
			return new ActivityResult("SF", e.getClass().getName() + ": " + e.getMessage(), (List) null);
		}
	}

	public ActivityResult changeSharedAccountPasswordWithTAMESSO(Account var1, String var2) {
		traceLogger.text(Level.DEBUG_MIN, "EncentuateCltAppExtension", "changeSharedAccountPasswordWithTAMESSO",
				"IBM Security Access Manager E-SSO Workflow Extension version 6.0.3.18");

		try {
			String var4 = "";
			if (this.util.hasTAMESSOPrerequisite(var1)) {
				ActivityResult var5 = this.imsAgent.encChangeSharedAccountPassword(var1);
				if (var5.getSummary().compareToIgnoreCase("SF") == 0) {
					var5.setSummary("SW");
					return var5;
				}

				if (var4 != null && !var4.equals("")) {
					var4 = var4 + " "
							+ "Change credential's password in ISAM E-SSO account's wallet completed successfully";
				} else {
					var4 = "Change credential's password in ISAM E-SSO account's wallet completed successfully";
				}
			} else {
				var4 = MSG_NO_PREREQ_FOUND;
			}

			return new ActivityResult(2, "SS", var4, (List) null);
		} catch (Exception var6) {
			traceLogger.exception(Level.DEBUG_MIN, "EncentuateCltAppExtension",
					"changeSharedAccountPasswordWithTAMESSO", var6);
			return new ActivityResult("SF", var6.getClass().getName() + ": " + var6.getMessage(), (List) null);
		}
	}

	public ActivityResult isSharedRole(Person var1) {
		traceLogger.text(Level.DEBUG_MID, "EncentuateCltAppExtension", "isSharedRole", "START ");
		String var3 = null;
		Collection var4 = null;
		String var5 = "false";
		Collection var6 = this.getRoleChangeCollection(var1);
		Collection var7 = this.getSharedRoleNames(var1);
		Iterator var8 = var6.iterator();

		label62: while (true) {
			AttributeValue var9;
			do {
				if (!var8.hasNext()) {
					traceLogger.text(Level.DEBUG_MIN, "EncentuateCltAppExtension", "isSharedRole", "sendEmail=" + var5);
					ArrayList var21 = new ArrayList();
					var21.add(var5);
					var21.add(new Person());
					return new ActivityResult(2, "SS", "No role found for the shared account", var21);
				}

				var9 = (AttributeValue) var8.next();
				var3 = var9.getName();
			} while (var3.compareToIgnoreCase("erroles") != 0);

			var4 = var9.getValues();
			Iterator var10 = var4.iterator();

			while (true) {
				while (true) {
					if (!var10.hasNext()) {
						continue label62;
					}

					String var11 = (String) var10.next();
					DistinguishedName var12 = new DistinguishedName(var11);
					Role var13 = this.util.getRole(var12);
					if (var13 != null) {
						String var14 = var13.getName();
						traceLogger.text(Level.DEBUG_MAX, "EncentuateCltAppExtension", "isSharedRole",
								"roleName=" + var14);
						int var15 = this.getModAction(var1);
						boolean var16 = this.isRoleOwner(var13, var1);
						Person var17 = this.getRoleOwner(var13);
						ArrayList var22;
						if (var17 == null) {
							var22 = new ArrayList();
							var22.add(var5);
							var22.add(new Person());
							return new ActivityResult(2, "SS", "Role doesn't have a owner", var22);
						}

						if (var16) {
							var5 = "false";
							traceLogger.text(Level.DEBUG_MIN, "EncentuateCltAppExtension", "isSharedRole",
									"sendEmail=" + var5);
							traceLogger.text(Level.DEBUG_MAX, "EncentuateCltAppExtension", "isSharedRole",
									"Recipient=" + var17.getName());
							var22 = new ArrayList();
							var22.add(var5);
							var22.add(var17);
							return new ActivityResult(2, "SS", "User is owner of the role for shared account ", var22);
						}

						Iterator var18 = var7.iterator();

						while (var18.hasNext()) {
							String var19 = (String) var18.next();
							if (var19 != null && var14.compareToIgnoreCase(var19) == 0) {
								traceLogger.text(Level.DEBUG_MAX, "EncentuateCltAppExtension", "isSharedRole",
										"A role is found for the shared account : " + var19);
								if (var15 == 3 && !var16) {
									traceLogger.text(Level.DEBUG_MAX, "EncentuateCltAppExtension", "isSharedRole",
											"Recipient=" + var17.getName());
									return this.removeSharedAccount(var1, var13);
								}

								if (var15 == 1 && !var16) {
									var5 = "true";
									traceLogger.text(Level.DEBUG_MIN, "EncentuateCltAppExtension", "isSharedRole",
											"sendEmail=" + var5);
									traceLogger.text(Level.DEBUG_MAX, "EncentuateCltAppExtension", "isSharedRole",
											"Recipient=" + var17.getName());
									ArrayList var20 = new ArrayList();
									var20.add(var5);
									var20.add(var17);
									return new ActivityResult(2, "SS", "A role is found for the shared account : ",
											var20);
								}
							}
						}
					} else {
						traceLogger.text(Level.DEBUG_MIN, "EncentuateCltAppExtension", "isSharedRole",
								"Role is not found");
					}
				}
			}
		}
	}

	private Collection getSharedRoleNames(Person var1) {
		traceLogger.text(Level.DEBUG_MID, "EncentuateCltAppExtension", "getSharedRoleNames", "START ");
		ArrayList var3 = new ArrayList();
		Collection var4 = this.util.findImsAccounts(var1.getDistinguishedName());
		Iterator var5 = var4.iterator();

		while (true) {
			Service var7;
			do {
				Account var6;
				do {
					if (!var5.hasNext()) {
						return var3;
					}

					var6 = (Account) var5.next();
				} while (var6 == null);

				var7 = this.task.getServiceForAccount(var6);
			} while (var7 == null);

			List var8 = this.util.getSharedAccountMapping(var7);

			for (int var9 = 0; var9 < var8.size(); ++var9) {
				String var10 = this.util.tokenizingSharedAccountMapping((String) var8.get(var9), 2);
				traceLogger.text(Level.DEBUG_MAX, "EncentuateCltAppExtension", "getSharedRoleNames",
						"sharedRole=" + var10);
				var3.add(var10);
			}
		}
	}

	private boolean isRoleOwner(Role var1, Person var2) {
		traceLogger.text(Level.DEBUG_MID, "EncentuateCltAppExtension", "isRoleOwner", "START ");
		Collection var4 = var1.getOwnerDNs();
		DistinguishedName var5 = var2.getDistinguishedName();
		Iterator var6 = var4.iterator();

		DistinguishedName var7;
		do {
			if (!var6.hasNext()) {
				return false;
			}

			var7 = (DistinguishedName) var6.next();
		} while (!var7.equals(var5));

		return true;
	}

	private Person getRoleOwner(Role var1) {
		traceLogger.text(Level.DEBUG_MID, "EncentuateCltAppExtension", "getRoleOwner", "START ");
		Person var3 = null;
		if (var1 == null) {
			return null;
		} else {
			try {
				Collection var4 = var1.getOwnerDNs();
				Iterator var5 = var4.iterator();
				if (var5.hasNext()) {
					DistinguishedName var6 = (DistinguishedName) var5.next();
					PersonEntity var7 = (new PersonSearch()).lookup(var6);
					var3 = (Person) var7.getDirectoryObject();
					traceLogger.text(Level.DEBUG_MAX, "EncentuateCltAppExtension", "getRoleOwner",
							"Role Owner=" + var3.getName());
					return var3;
				}
			} catch (ModelCommunicationException var8) {
				traceLogger.exception(Level.DEBUG_MIN, "EncentuateCltAppExtension", "getRoleOwner", var8);
			} catch (ObjectNotFoundException var9) {
				traceLogger.exception(Level.DEBUG_MIN, "EncentuateCltAppExtension", "getRoleOwner", var9);
			}

			return null;
		}
	}

	private int getModAction(Person var1) {
		traceLogger.text(Level.DEBUG_MID, "EncentuateCltAppExtension", "getModAction", "START ");
		int var3 = 0;
		AttributeChangeOperation var4 = var1.getChanges().get(Person.PERSON_ATTR_ROLE);
		if (var4 != null) {
			var3 = var4.getModificationAction();
		}

		return var3;
	}

	private Collection getRoleChangeCollection(Person var1) {
		traceLogger.text(Level.DEBUG_MID, "EncentuateCltAppExtension", "getRoleChangeCollection", "START ");
		Object var3 = new ArrayList();
		AttributeChangeOperation var4 = var1.getChanges().get(Person.PERSON_ATTR_ROLE);
		if (var4 != null) {
			int var5 = var4.getModificationAction();
			if (var5 == 1 || var5 == 3) {
				var3 = var4.getChangeData();
			}
		}

		return (Collection) var3;
	}

	private boolean hasImsAccountPerService(Person var1, Service var2) {
		traceLogger.text(Level.DEBUG_MID, "EncentuateCltAppExtension", "hasImsAccountPerService", "START ");
		DistinguishedName var4 = var2.getDistinguishedName();
		Collection var5 = this.util.findImsAccounts(var1.getDistinguishedName());
		Iterator var6 = var5.iterator();

		Account var7;
		do {
			if (!var6.hasNext()) {
				return false;
			}

			var7 = (Account) var6.next();
		} while (!this.util.isSameService(var4, var7.getServiceDN()));

		traceLogger.text(Level.DEBUG_MAX, "EncentuateCltAppExtension", "hasImsAccountPerService",
				"Found existing ISAMESSO Account : " + var7.getUserId());
		return true;
	}

	private ActivityResult removeSharedAccount(Person var1, Role var2) {
		traceLogger.text(Level.DEBUG_MIN, "EncentuateCltAppExtension", "removeSharedAccount", "START ");
		String var4 = "false";
		DistinguishedName var5 = var1.getDistinguishedName();
		Object var6 = null;
		Person var7 = this.getRoleOwner(var2);
		if (var7 == null) {
			ArrayList var20 = new ArrayList();
			var20.add(var4);
			var20.add(new Person());
			return new ActivityResult(2, "SS", "Role doesn't have a owner", var20);
		} else {
			traceLogger.text(Level.DEBUG_MAX, "EncentuateCltAppExtension", "removeSharedAccount",
					"Recipient=" + var7.getName());
			Collection var8 = this.util.findImsAccounts(var5);
			if (var8 == null || var8.size() == 0) {
				traceLogger.text(Level.DEBUG_MAX, "EncentuateCltAppExtension", "removeSharedAccount",
						"ISAM ESSO account is not found");
			}

			Iterator var9 = var8.iterator();

			while (true) {
				Account var10;
				Service var11;
				do {
					do {
						if (!var9.hasNext()) {
							traceLogger.text(Level.DEBUG_MIN, "EncentuateCltAppExtension", "removeSharedAccount",
									"sendEmail=" + var4);
							traceLogger.text(Level.DEBUG_MAX, "EncentuateCltAppExtension", "removeSharedAccount",
									"Recipient=" + var7.getName());
							ArrayList var21 = new ArrayList();
							var21.add(var4);
							var21.add(var7);
							return new ActivityResult(2, "SS",
									"Failed to remove shared account from ISAM ESSO account's wallet", var21);
						}

						var10 = (Account) var9.next();
					} while (var10 == null);

					var11 = this.task.getServiceForAccount(var10);
				} while (var11 == null);

				List var12 = this.util.getSharedAccountMapping(var11);

				for (int var13 = 0; var13 < var12.size(); ++var13) {
					String var14 = this.util.tokenizingSharedAccountMapping((String) var12.get(var13), 0);
					String var15 = this.util.tokenizingSharedAccountMapping((String) var12.get(var13), 1);
					String var16 = this.util.tokenizingSharedAccountMapping((String) var12.get(var13), 2);
					String var17 = var2.getName();
					if (var16.compareToIgnoreCase(var17) == 0 && var14 != null && var15 != null) {
						Account var18 = this.getSharedAccountForRole(var2, var14, var15);
						if (var18 != null && this.task.taskDeleteAcc(var10, var18)) {
							var4 = "true";
							traceLogger.text(Level.DEBUG_MIN, "EncentuateCltAppExtension", "removeSharedAccount",
									"sendEmail=" + var4);
							traceLogger.text(Level.DEBUG_MAX, "EncentuateCltAppExtension", "removeSharedAccount",
									"Recipient=" + var7.getName());
							ArrayList var19 = new ArrayList();
							var19.add(var4);
							var19.add(var7);
							return new ActivityResult(2, "SS",
									" Shared account is removed from ISAM ESSO account's wallet", var19);
						}
					}
				}
			}
		}
	}

	private Account getSharedAccountForRole(Role var1, String var2, String var3) {
		traceLogger.text(Level.DEBUG_MIN, "EncentuateCltAppExtension", "getSharedAccountForRole", "START ");

		try {
			Collection var5 = var1.getOwnerDNs();
			Iterator var6 = var5.iterator();

			while (var6.hasNext()) {
				DistinguishedName var7 = (DistinguishedName) var6.next();
				Collection var8 = (new AccountSearch()).searchByOwner(var7);
				Iterator var9 = var8.iterator();

				while (var9.hasNext()) {
					AccountEntity var10 = (AccountEntity) var9.next();
					Account var11 = (Account) var10.getDirectoryObject();
					Service var12 = this.task.getServiceForAccount(var11);
					if (var12 != null && var11.getName().compareToIgnoreCase(var3) == 0
							&& var12.getName().compareToIgnoreCase(var2) == 0) {
						return var11;
					}
				}
			}
		} catch (ModelCommunicationException var13) {
			traceLogger.exception(Level.DEBUG_MIN, "EncentuateCltAppExtension", "getSharedAccountForRole", var13);
		} catch (ObjectNotFoundException var14) {
			traceLogger.exception(Level.DEBUG_MIN, "EncentuateCltAppExtension", "getSharedAccountForRole", var14);
		}

		return null;
	}

	public ActivityResult noExistingSSOAccount(Person var1, Service var2, Account var3) {
		traceLogger.text(Level.DEBUG_MIN, "EncentuateCltAppExtension", "noExistingSSOAccount",
				"IBM Security Access Manager E-SSO Workflow Extension version 6.0.3.18");

		try {
			return this.hasImsAccountPerService(var1, var2)
					? new ActivityResult("SF", "Only one ISAM E-SSO account per service is allowed.", (List) null)
					: new ActivityResult("SS", "", (List) null);
		} catch (Exception var6) {
			return new ActivityResult("SF", var6.getLocalizedMessage(), (List) null);
		}
	}
}