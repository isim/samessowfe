package encentuate.bridges.wfe;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import com.ibm.itim.common.AttributeValue;
import com.ibm.itim.dataservices.model.DistinguishedName;
import com.ibm.itim.dataservices.model.ModelCommunicationException;
import com.ibm.itim.dataservices.model.ObjectNotFoundException;
import com.ibm.itim.dataservices.model.domain.Account;
import com.ibm.itim.dataservices.model.domain.AccountEntity;
import com.ibm.itim.dataservices.model.domain.AccountSearch;
import com.ibm.itim.dataservices.model.domain.Person;
import com.ibm.itim.dataservices.model.domain.PersonEntity;
import com.ibm.itim.dataservices.model.domain.PersonSearch;
import com.ibm.itim.dataservices.model.domain.Role;
import com.ibm.itim.dataservices.model.domain.RoleEntity;
import com.ibm.itim.dataservices.model.domain.RoleSearch;
import com.ibm.itim.dataservices.model.domain.Service;
import com.ibm.itim.dataservices.model.domain.ServiceEntity;
import com.ibm.itim.dataservices.model.domain.ServiceSearch;
import com.ibm.itim.logging.JLogUtil;
import com.ibm.itim.remoteservices.provider.itdiprovider.ItdiServiceProvider;
import com.ibm.log.Level;
import com.ibm.log.PDLogger;

public class Util {
	private static String TamessoAccountName = "TAMESSOAccount";
	private static String TamessoServiceProfileName = "tamessoprofile";
	private static final String ROLE_SHARED_ACCOUNT_MAPPING = "ertamessosharedmap";
	private static final String SHARED_SERVICE = "SharedService=";
	private static final String SHARED_ACCOUNT = "SharedAccount=";
	private static final String SHARED_ROLE = "SharedRole=";
	private static final String MSG_TAMESSO_ACC_FOUND = "Found existing ISAMESSO Account : ";
	private static final String MSG_TAMESSO_SERVICE_FOUND = "Found existing ISAMESSO Service : ";
	private static final String MSG_SHARED_ROLE = "Shared Role=";
	private static final String MSG_SHARED_ACCOUNT_MAPPING = "The shared account mapping is : ";
	private static final String MSG_SHARED_ROLE_FOUND = "A shared account role is found: ";
	private static final String ERR_SHARED_ACCOUNT_MAPPING = "shared account mapping is not configured correctly ";
	private static final String ERR_SHARED_ACCOUNT_MAPPING_NOT_FOUND = "shared account mapping is not found ";
	private static final String ERR_NOT_SHARED_ACCOUNT = " is not a shared account. ";
	private Task task = new Task();
	private static final PDLogger traceLogger = JLogUtil.getTraceLogger(ItdiServiceProvider.class);
	static final String CLASS_NAME = "Util";
	private String shareRoleName;

	public boolean isImsAccount(String profileName) {
		traceLogger.text(Level.DEBUG_MAX, "Util", "isImsAccount", "START ");
		return profileName.equalsIgnoreCase(TamessoAccountName);
	}

	public Account getImsAccount(Service service, DistinguishedName personDn) {
		traceLogger.text(Level.DEBUG_MAX, "Util", "getImsAccount", "START ");
		Collection servicePrerequisite = service.getPrerequisiteDNs();
		Collection imsAccounts = this.findImsAccounts(personDn);
		Iterator servicePrerequisiteIterator = servicePrerequisite.iterator();

		Iterator imsAccountsIterator = imsAccounts.iterator();

		while (imsAccountsIterator.hasNext()) {
			Account isamAccount = (Account) imsAccountsIterator.next();
			traceLogger.text(Level.DEBUG_MAX, "Util", "getImsAccount",
					"Found existing ISAMESSO Account : " + isamAccount.getUserId());
			return isamAccount;
		}

		return null;
	}

	public boolean isSameService(DistinguishedName var1, DistinguishedName var2) {
		traceLogger.text(Level.DEBUG_MAX, "Util", "isSameService", "START ");
		return var1.toString().equalsIgnoreCase(var2.toString());
	}

	public Collection findImsAccounts(DistinguishedName personDn) {
		traceLogger.text(Level.DEBUG_MAX, "Util", "findImsAccounts", "START ");
		ArrayList imsAccounts = new ArrayList();

		try {
			Collection accounts = (new AccountSearch()).searchByOwner(personDn);
			Iterator accountsIterator = accounts.iterator();

			while (accountsIterator.hasNext()) {
				AccountEntity var6 = (AccountEntity) accountsIterator.next();
				Account account = (Account) var6.getDirectoryObject();
				if (this.isImsAccount(account.getProfileName())) {
					traceLogger.text(Level.DEBUG_MAX, "Util", "findImsAccounts",
							"Found existing ISAMESSO Account : " + account.getUserId());
					imsAccounts.add(account);
				}
			}
		} catch (ModelCommunicationException var8) {
			traceLogger.exception(Level.DEBUG_MIN, "Util", "findImsAccounts", var8);
		} catch (ObjectNotFoundException var9) {
			traceLogger.exception(Level.DEBUG_MIN, "Util", "findImsAccounts", var9);
		}

		return imsAccounts;
	}

	public boolean isImsService(String var1) {
		traceLogger.text(Level.DEBUG_MAX, "Util", "isImsService", "START ");
		this.LogDebug(Level.DEBUG_MIN, "Util", "isImsService",
				"Comparing Profiles: " + var1 + " == " + TamessoServiceProfileName);
		return var1.equalsIgnoreCase(TamessoServiceProfileName);
	}

	private void LogDebug(Level var1, String var2, String var3, String var4) {
		traceLogger.text(var1, var2, var3, var4);
	}

	public boolean hasTAMESSOPrerequisite(Account account) {
		traceLogger.text(Level.DEBUG_MAX, "Util", "hasTAMESSOPrerequisite", "START ");
		Service service = this.task.getServiceForAccount(account);
		this.LogDebug(Level.DEBUG_MIN, "Util", "hasTAMESSOPrerequisite", "Got service: " + service.getName());
		
		if (service != null) {
			this.LogDebug(Level.DEBUG_MIN, "Util", "hasTAMESSOPrerequisite", "Passed service checks");
			return true;
		}

		return false;
	}

	public Service findTAMESSOPrerequisite(Service var1) {
		traceLogger.text(Level.DEBUG_MAX, "Util", "findTAMESSOPrerequisite", "START ");
		if (var1 != null && !var1.getPrerequisiteDNs().isEmpty()) {
			Collection var3 = var1.getPrerequisiteDNs();
			Iterator var4 = var3.iterator();

			while (var4.hasNext()) {
				DistinguishedName var5 = (DistinguishedName) var4.next();

				try {
					ServiceEntity var6 = (new ServiceSearch()).lookup(var5);
					Service var7 = (Service) var6.getDirectoryObject();
					if (this.isImsService(var7.getProfileName())) {
						traceLogger.text(Level.DEBUG_MAX, "Util", "findTAMESSOPrerequisite",
								"Found existing ISAMESSO Service : " + var7.getName());
						return var7;
					}
				} catch (ObjectNotFoundException var8) {
					traceLogger.exception(Level.DEBUG_MIN, "Util", "findTAMESSOPrerequisite", var8);
					return null;
				} catch (ModelCommunicationException var9) {
					traceLogger.exception(Level.DEBUG_MIN, "Util", "findTAMESSOPrerequisite", var9);
					return null;
				}
			}
		}

		return null;
	}

	public boolean isSharedAccount(Account var1, Service var2) {
		traceLogger.text(Level.DEBUG_MAX, "Util", "isSharedAccount", "START ");
		Service var4 = this.task.getServiceForAccount(var1);
		if (var4 != null) {
			String var5 = var4.getName();
			List var6 = this.getSharedAccountMapping(var2);

			for (int var7 = 0; var7 < var6.size(); ++var7) {
				String var8 = this.tokenizingSharedAccountMapping((String) var6.get(var7), 0);
				String var9 = this.tokenizingSharedAccountMapping((String) var6.get(var7), 1);
				String var10 = this.tokenizingSharedAccountMapping((String) var6.get(var7), 2);
				traceLogger.text(Level.DEBUG_MAX, "Util", "isSharedAccount", "SharedService=" + var8,
						"|SharedAccount=" + var9, "|SharedRole=" + var10);
				if (var1.getUserId().compareToIgnoreCase(var9) == 0 && var5.compareToIgnoreCase(var8) == 0) {
					this.setShareRole(var10);
					return true;
				}

				traceLogger.text(Level.DEBUG_MIN, "Util", "isSharedAccount", var9 + " is not a shared account. ");
			}
		}

		return false;
	}

	private String getShareRole() {
		return this.shareRoleName;
	}

	private void setShareRole(String var1) {
		traceLogger.text(Level.DEBUG_MAX, "Util", "setShareRole", "START ");
		this.shareRoleName = var1;
		traceLogger.text(Level.DEBUG_MAX, "Util", "setShareRole", "Shared Role=" + var1);
	}

	public RoleEntity getRoleforSharedAccount(Account var1) {
		traceLogger.text(Level.DEBUG_MAX, "Util", "getRoleforSharedAccount", "START ");
		RoleEntity var3 = null;

		try {
			PersonSearch var4 = new PersonSearch();
			PersonEntity var5 = var4.lookup(var1.getOwnerDN());
			Person var6 = (Person) var5.getDirectoryObject();
			Collection var7 = var6.getRoles();
			Iterator var8 = var7.iterator();

			while (var8.hasNext()) {
				DistinguishedName var9 = (DistinguishedName) var8.next();
				Role var10 = this.getRole(var9);
				if (var10 != null) {
					String var11 = var10.getName();
					String var12 = this.getShareRole();
					if (var11.compareToIgnoreCase(var12) == 0) {
						traceLogger.text(Level.DEBUG_MAX, "Util", "getRoleforSharedAccount",
								"A shared account role is found: " + var11);
						var3 = new RoleEntity(var10);
						break;
					}
				}
			}
		} catch (ModelCommunicationException var13) {
			traceLogger.exception(Level.DEBUG_MIN, "Util", "getRoleforSharedAccount", var13);
		} catch (ObjectNotFoundException var14) {
			traceLogger.exception(Level.DEBUG_MIN, "Util", "getRoleforSharedAccount", var14);
		}

		return var3;
	}

	public Role getRole(DistinguishedName var1) {
		traceLogger.text(Level.DEBUG_MAX, "Util", "getRole", "START ");
		if (var1 == null) {
			return null;
		} else {
			try {
				RoleSearch var3 = new RoleSearch();
				RoleEntity var4 = var3.lookup(var1);
				Role var5 = (Role) var4.getDirectoryObject();
				traceLogger.text(Level.DEBUG_MAX, "Util", "getRole",
						"Role is " + (var5 == null ? "null" : var5.getName()));
				return var5;
			} catch (ModelCommunicationException var6) {
				traceLogger.exception(Level.DEBUG_MIN, "Util", "getRole", var6);
			} catch (ObjectNotFoundException var7) {
				traceLogger.exception(Level.DEBUG_MIN, "Util", "getRole", var7);
			}

			return null;
		}
	}

	public List getSharedAccountMapping(Service var1) {
		traceLogger.text(Level.DEBUG_MAX, "Util", "getSharedAccountMapping", "START ");
		ArrayList var3 = new ArrayList();
		AttributeValue var4 = var1.getAttribute("ertamessosharedmap");
		if (var4 != null) {
			Collection var5 = var4.getValues();
			if (var5 != null) {
				Iterator var6 = var5.iterator();

				while (var6.hasNext()) {
					String var7 = (String) var6.next();
					if (var7 != null) {
						var3.add(var7);
						traceLogger.text(Level.DEBUG_MAX, "Util", "getSharedAccountMapping",
								"The shared account mapping is : " + var7);
					}
				}

				return var3;
			}

			traceLogger.text(Level.DEBUG_MIN, "Util", "getSharedAccountMapping",
					"shared account mapping is not found ");
		} else {
			traceLogger.text(Level.DEBUG_MIN, "Util", "getSharedAccountMapping",
					"shared account mapping is not found ");
		}

		return var3;
	}

	public String tokenizingSharedAccountMapping(String var1, int var2) {
		traceLogger.text(Level.DEBUG_MAX, "Util", "tokenizingSharedAccountMapping", "START ");
		StringTokenizer var4 = new StringTokenizer(var1, "|");
		int var5 = 0;

		String var6;
		for (var6 = null; var4.hasMoreTokens() && var5 <= var2; ++var5) {
			var6 = var4.nextToken();
		}

		if (var6 == null || var5 < var2) {
			traceLogger.text(Level.DEBUG_MIN, "Util", "tokenizingSharedAccountMapping",
					"shared account mapping is not configured correctly ");
			var6 = null;
		}

		return var6;
	}

	public static boolean isNullOrEmpty(String var0) {
		if (var0 == null) {
			return true;
		} else {
			return var0.trim().length() < 1;
		}
	}
}