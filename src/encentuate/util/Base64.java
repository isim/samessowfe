package encentuate.util;

public final class Base64 {
	public static String encode(String var0) {
		try {
			return encodeBytes(var0.getBytes("UTF-8"));
		} catch (Exception var2) {
			return "";
		}
	}

	public static String encodeBytes(byte[] var0) {
		try {
			return new String(java.util.Base64.getEncoder().encode(var0));
		} catch (Exception var3) {
			return "";
		}
	}

	public static String decodeToString(String var0) {
		try {
			byte[] var1 = decode(var0);
			return new String(var1, "UTF-8");
		} catch (Exception var2) {
			return "";
		}
	}

	public static byte[] decode(String var0) {
		try {
			return java.util.Base64.getDecoder().decode(var0);
		} catch (Exception var3) {
			return null;
		}
	}
}